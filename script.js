var default_content="";

var listaManejo = [
	{
		"id": 1,
		"nombre" : "Luis David",
		"relacionalEntity": 21,
		"relacionalEntitytype": "21",
		"responsibleUserId" : {"id" : 1 },
		"Carrera" : { "id" : "Desarrollador"},
		"nombreType": {"id" : 1},
		"nombrestateid" : {"id" : 1 },
		"companyId" : {"id" : 1 }
	},
	{
		"id": 2,
		"nombre" : "Cristian",
		"relacionalEntity": 22,
		"relacionalEntitytype": "21",
		"responsibleUserId" : {"id" : 1 },
		"Carrera" : { "id" : "Arquitecto"},
		"nombreType": {"id" : 1},
		"nombrestateid" : {"id" : 1 },
		"companyId" : {"id" : 1 }
	},
	{
		"id": 3,
		"nombre" : "Tatiana",
		"relacionalEntity": 23,
		"relacionalEntitytype": "21",
		"responsibleUserId" : {"id" : 1 },
		"Carrera" : { "id" : "Diseñadora Grafica"},
		"nombreType": {"id" : 1},
		"nombrestateid" : {"id" : 1 },
		"companyId" : {"id" : 1 }
	}
]

$(document).ready(function(){

	traerData();
	checkURL();
	$('ul li a').click(function (e){

			checkURL(this.hash);

	});

	$("#btnSave").click(function (e){
		let Estudiante = $("#nombre").val();
		if (Estudiante != ""){

		pushLista(Estudiante);

		} else {
			alert("Llene la información");
		}
		

});
	
	//filling in the default content
	default_content = $('#pageContent').html();
	
	
	setInterval("checkURL()",250);
	
});

function pushLista(Estudiante){

	let Carrera = $("#Carrera").val();
	let idCampo = $("#idReg").val();
	let pos =  $("#idPos").val();
	let n;
	let acceso = false;

	if (idCampo == ""){
		n = (listaManejo[listaManejo.length - 1]) ? listaManejo[listaManejo.length - 1].id : 0;
		n = Number(n) + 1;
	} else {
		n = idCampo;
		acceso = true;
	}

	var registro = {
		"id": n,
		"nombre" : Estudiante,
		"relacionalEntity": 26,
		"relacionalEntitytype": "21",
		"responsibleUserId" : {"id" : 1 },
		"Carrera" : { "id" : Carrera},
		"nombreType": {"id" : 1},
		"nombrestateid" : {"id" : 1 },
		"companyId" : {"id" : 1 }
		
	}
	if (acceso) {
		listaManejo[pos] = registro;
	} else {
		listaManejo.push(registro);
	}
	$("#btnSave").html('Guardar')
	traerData();
}

var lasturl="";

function checkURL(hash)
{
	if(!hash) hash=window.location.hash;
	
	if(hash != lasturl)
	{
		lasturl=hash;
		
		// FIX - if we've used the history buttons to return to the homepage,
		// fill the pageContent with the default_content
		
		if(hash=="")
		$('#pageContent').html(default_content);
		
		else
		loadPage(hash);
	}
}

function eliminar(e){
	listaManejo.splice(e, 1);
	traerData();
}

function editar(e){
	var nueva = listaManejo.slice(e,e+1);
	$("#nombre").val(nueva[0].nombre);
	$("#Carrera").val(nueva[0].Carrera.id);
	$("#idReg").val(nueva[0].id);
	$("#idPos").val(e);
	$("#btnSave").html('Actualizar')
}

function traerData(){
	var tabla = "<table class='table thead-dark'>";
	tabla += "<thead class='thead-dark'><tr>"
			+"<th>ID</th>"
			+"<th>Estudiante</th>"
			+"<th>Carrera Profesional</th>"
			+"<th><th>"
			+"</tr></thead>";

	let count = 0;
	listaManejo.forEach(
		(listItem) => {
			
			tabla += '<tr><td>'+listItem.id+'</td>'
					+'<td>'+listItem.nombre+'</td>'
					+'<td>'+listItem.Carrera.id+'</td>'
					+'<td><button class="btn btn-success" onclick="editar('+count+')">Editar</button></td>'
					+'<td><button class="btn btn-danger" onclick="eliminar('+count+')">Eliminar</button></td>'
					+'</tr>';
					count++;
		}
	)

	tabla += "</table>";
	$("#idReg").val('')
	$("#nombre").val('')
	$("#Carrera").val('')

	
	$("#tablaNuestra").html(tabla)
}


function loadPage(url)
{
	// url=url.replace('#page','');
	
	// $('#loading').css('visibility','visible');
	
	// $.ajax({
	// 	type: "POST",
	// 	url: "load_page.php",
	// 	data: 'page='+url,
	// 	dataType: "html",
	// 	success: function(msg){
			
	// 		if(parseInt(msg)!=0)
	// 		{
	// 			$('#pageContent').html(msg);
	// 			$('#loading').css('visibility','hidden');
	// 		}
	// 	}
		
	// });

}